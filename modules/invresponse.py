#!/usr/bin/env python
# this is <invresponse.py>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2023 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# module to handle inventory data
# 
# ----
# This program source code is licensed under a CC0 license.
# 
# To the extent possible under law, the author(s) have waived all copyright
# and related or neighboring rights to this source code. You can copy, modify,
# distribute and compile the code, even for commercial purposes, all without
# asking permission. 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# 
# For the complete text of the license, please visit
# https://creativecommons.org/publicdomain/zero/1.0/
# ----
#
# REVISIONS and CHANGES 
#    22/09/2023   V1.0   Thomas Forbriger
# 
# ============================================================================
#

import numpy as np
from obspy import read, read_inventory
from obspy.core import AttribDict, UTCDateTime
from obspy.geodetics.base import locations2degrees
import matplotlib.pyplot as plt

def dumpresp(resp):
    """
    resp: obspy response object obtained through get_response function of
    inventory
    """
    print(resp)
    seis_paz=resp.get_paz()
    print(seis_paz)
    print("'"+seis_paz.pz_transfer_function_type+"'")
# factor to scale to units of Hertz (set zo 0, if units are unknown)
    funits=0.
    if seis_paz.pz_transfer_function_type == "LAPLACE (HERTZ)":
        funits=1
    elif seis_paz.pz_transfer_function_type == "LAPLACE (RADIANS/SECOND)":
        funits=0.5/np.pi
    else:
        print("ERROR: unexpected type of response parameters!")
    paz_dict={"zeros": seis_paz.zeros, 
              "poles": seis_paz.poles, 
              "gain": seis_paz.stage_gain}
    print(paz_dict)
    for t in ("zeros", "poles"):
        print("\nlist of %s:" % t)
        for x in paz_dict[t]:
            f=np.abs(x)*funits
            if ((f < 1.e-10) or (f >= 1.)):
                fstring="%10.4f Hz" % f
            else:
                fstring="%10.4f s" % (1./f)
            print(fstring, "at", x*funits, "Hz")

# ----- END OF invresponse.py ----- 
