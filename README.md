Quick hacks to analyse VLP signal
=================================

First run

    dldata.sh

to download data.

Then run

    pmplots.sh

to create particle motion diagrams.

Send bug reports and complaints to
Thomas Forbriger (thomas.forbriger@kit.edu)
