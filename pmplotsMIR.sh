#!/bin/sh
# this is <pmplots1st.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2023 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# particle motion plots for first overtone at 21.6 mHz
# 
# ----
# This program source code is licensed under a CC0 license.
# 
# To the extent possible under law, the author(s) have waived all copyright
# and related or neighboring rights to this source code. You can copy, modify,
# distribute and compile the code, even for commercial purposes, all without
# asking permission. 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# 
# For the complete text of the license, please visit
# https://creativecommons.org/publicdomain/zero/1.0/
# ----
#
# REVISIONS and CHANGES 
#    30/09/2023   V1.0   Thomas Forbriger
# 
# ============================================================================
#
#
VERSION=2023-09-30

EBEGIN=2023/09/16T12:30:00
EEND=2023/09/16T14:00:00
BEGIN=2023/09/16T14:00:00
END=2023/09/16T15:45:00
LBEGIN=2023/09/16T15:30:00
LEND=2023/09/16T17:30:00
#END=2023/09/16T20:00:00

doplot() {
  # plot signal from M5.4 eq at Mid Indian Ridge
  # centered on the epicenter of this earthquake
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $BEGIN \
    --end $END \
    --source=-13.76,66.22 \
    --outbase pmMIREQ \
    --fHP 0.015 \
    --fLP 0.030 --noshow
}

doplot IU KEV
doplot IU SSPA
doplot IU SFJD
doplot II ALE
doplot II BORG
doplot IU TIXI
doplot DK SCO
doplot GE SUMG
doplot II BFO
doplot IU ANMO
doplot IU ULN
doplot IU PAB
doplot IU KONO
doplot IU COLA
doplot IU KBS

# ----- END OF pmplots1st.sh ----- 
