#!/bin/sh
# this is <pmplots.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2023 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# particle motion plots
# 
# ----
# This program source code is licensed under a CC0 license.
# 
# To the extent possible under law, the author(s) have waived all copyright
# and related or neighboring rights to this source code. You can copy, modify,
# distribute and compile the code, even for commercial purposes, all without
# asking permission. 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# 
# For the complete text of the license, please visit
# https://creativecommons.org/publicdomain/zero/1.0/
# ----
#
# REVISIONS and CHANGES 
#    30/09/2023   V1.0   Thomas Forbriger
# 
# ============================================================================
#
#
VERSION=2023-09-30

BEGIN=2023/09/16T12:30:00
END=2023/09/16T14:00:00
#END=2023/09/16T20:00:00

doplot() {
  ./plotstation.py --verbose --quantity displacement $1 $2 \
    --begin $BEGIN \
    --end $END \
    --inbase $2$3 \
    --outbase pmplot$3  \
    --fHP 0.005 \
    --fLP 0.020 --noshow
  ./plotstation.py --verbose --quantity displacement $1 $2 \
    --begin $BEGIN \
    --end $END \
    --inbase $2$3 \
    --outbase pmZNE$3 --ZNE \
    --fHP 0.005 \
    --fLP 0.020 --noshow
}

doplot II KDAK 00
doplot II KDAK 10

# ----- END OF pmplots.sh ----- 
