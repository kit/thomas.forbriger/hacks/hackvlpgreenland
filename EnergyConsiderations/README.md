# Considerations for the dynamics of the VLP source
This computation shall check whether the force amplitude for the VLP source as
inferred from teleseismic recordings would be consistent with the simulated
seiche amplitude.

## Files
### `FjordSeicheGeometry.jpg`
Hand-drawn sketch of the cross-section of the water-body in the fjord.
This is used to find the location of the center of mass.

### `FjordGeometry.py`
Python script to create a sketch of the fjord geometry.

### `VLPsource_dynamics_triangular.ipynb`
Computation of amplitudes and energy for a seiche oscillation in the enire
water-body shaped by a triangular geometry of the cross-section of the fjord.

## Parameters
Collection of essential input parameters.
Not all of them are used in the current computation.

### Values taken from the manuscript
landslide origin time:  12:35 UT  
rock mass:  25.5 +- 1.7 e6 m³  
height of collapsed rock:     1200m above Dickson Fjrod  
glacial mass in avalanche: 2.3 +- 0.15 e6 m³  
Mountain collapse location: 72.81°N 26.95°WS   
Dickson Fjord location at gully: 72.833°N 27.00°W  
Dickson Fjord location: 72.78°N, 27.11°W  
Fjord depth near gully: 540m  
Fjord width near gully: 2700m  
initial runup height: 200m  
initial tsunami height: 100m  
from simulation: initial seiche vertical amplitude at shore: 5m-10m  
seismic source origin time: 2023-09-16T12:35:03 UT  
Q-value of simulated transverse seiche: 226  
initial Q-value of VLP-signal: 500  
final Q-value of VLP-signal: 3000  

#### landslide seismic source
  192e9 N  
  78-103 e9 kg  
  55e9 kg with runout distance of 2.2km  

### Further parameters
#### rock density
The bedrocks are paragneiss/metamsediments with density around 2.8 to 2.9
g/cm³

#### length of involved section of Dickson Fjord
10 km appear to be a reasonable distance

along-fjord dimensions:
[on mattermost](http://beerstorming.be:8080/vlpgreenland/pl/woxu6sus8pgajgx73p9p3huq5e)  
transverse seiche in the simulation:
[on mattermost](http://beerstorming.be:8080/vlpgreenland/pl/3n8ooqk1spbmbck118ug94gu5h)  
