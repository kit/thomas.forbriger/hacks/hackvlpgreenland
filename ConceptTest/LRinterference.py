#!/usr/bin/env python
# this is <LRinterference.py>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2023 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# Test interference of Love- and Rayleigh-waves
# 
# REVISIONS and CHANGES 
#    11/10/2023   V1.0   Thomas Forbriger
# 
# ============================================================================
#
import numpy as np
import matplotlib.pyplot as plt
#
# Love-velocity / km/s
vL=4.4
# Rayleigh-velocity / km/s
vR=4.1
# minimum distance / km
mindist=3000.
# sampling interval for distance / km
dx=200.
# number of diagrams
nrows=3
ncols=5
ndiag=nrows*ncols
# maximum distance / km
maxdist=mindist+dx*(ndiag-1)

# signal period / s
T=92.
# time series sampling interval / s
dt=1.
# number of samples
nsamples=3600
# time values
t=dt*np.arange(nsamples, dtype=float)

paralabel=("%s: %5.1fs   %s: %4.1f km/s   %s: %4.1f km/s" %
           ("signal period", T, 
            "Love-velocity", vL,
            "Rayleigh-velocity", vR))

# plot phase lag
fig = plt.figure(figsize=(18./2.54,12./2.54),
                 layout="constrained")
x=np.arange(mindist, maxdist, 1., dtype=float)
plt.plot(x,x*(1./vR-1./vL)/T)
plt.title("phase shift between Love- and Rayleigh-waves\n"+paralabel)
plt.xlabel("distance / km")
plt.ylabel("phase shift / 2 $\pi$")
fig.savefig("LRphasedifference.pdf")
fig.savefig("LRphasedifference.png")
plt.show()

# plot particle motion
fig = plt.figure(figsize=(29./2.54,20./2.54),
                 layout="constrained")

for l in range(ndiag):
    dist=mindist+dx*l
    # Rayleigh wave signal
    R=np.sin(2.*np.pi*(t-dist/vR)/T)
    # Love wave signal
    L=np.sin(2.*np.pi*(t-dist/vL)/T)
    ax=fig.add_subplot(nrows,ncols,l+1)
    ax.plot(R, L)
    ax.set_xlabel("radial (Rayleigh)")
    ax.set_ylabel("transverse (Love)")
    ax.grid()
    ax.set_title("distance: %6.1fkm" % dist)

axs=fig.get_axes()
#print(type(axs[0]))
for ax in axs:
    ax.set_aspect("equal", adjustable="box")

fig.suptitle(paralabel)

fig.savefig("LRinterference.pdf")
fig.savefig("LRinterference.png")
plt.show()

# ----- END OF LRinterference.py ----- 
