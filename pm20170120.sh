#!/bin/sh
# this is <pmplots.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2023 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# particle motion plots
# 
# ----
# This program source code is licensed under a CC0 license.
# 
# To the extent possible under law, the author(s) have waived all copyright
# and related or neighboring rights to this source code. You can copy, modify,
# distribute and compile the code, even for commercial purposes, all without
# asking permission. 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# 
# For the complete text of the license, please visit
# https://creativecommons.org/publicdomain/zero/1.0/
# ----
#
# REVISIONS and CHANGES 
#    30/09/2023   V1.0   Thomas Forbriger
# 
# ============================================================================
#
#
VERSION=2023-09-30

BEGIN=2017/01/20T07:20:00
END=2017/01/20T11:50:00
#END=2023/09/16T20:00:00
DATEBASE=20170120
SOURCELOC=72.8,-27.0

doplot() {
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $BEGIN \
    --inbase ${DATEBASE} \
    --outbase pm${DATEBASE} \
    --source=$SOURCELOC \
    --end $END \
    --fHP 0.005 \
    --fLP 0.020 --noshow
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $BEGIN \
    --inbase ${DATEBASE} \
    --outbase pm${DATEBASE}ZNE --ZNE \
    --source=$SOURCELOC \
    --end $END \
    --fHP 0.005 \
    --fLP 0.020 --noshow
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $BEGIN \
    --end $END \
    --inbase ${DATEBASE} \
    --outbase pm${DATEBASE}narrow \
    --source=$SOURCELOC \
    --fHP 0.010 \
    --fLP 0.012 --noshow
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $BEGIN \
    --inbase ${DATEBASE} \
    --outbase pm${DATEBASE}ZNEnarrow --ZNE \
    --source=$SOURCELOC \
    --end $END \
    --fHP 0.010 \
    --fLP 0.012 --noshow
}

doplot GR BFO
doplot DK SCO
doplot II BORG
doplot IU SFJD
doplot IU KEV
doplot IU KONO
doplot II KDAK
doplot IU TIXI
doplot II ALE
doplot II BFO
doplot IU ANMO
doplot IU ULN
doplot IU PAB
doplot IU COLA
doplot IU KBS
doplot IU SSPA

# ----- END OF pmplots.sh ----- 
