#!/bin/sh
# this is <dldata.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2023 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# download some data
# 
# ----
# This program source code is licensed under a CC0 license.
# 
# To the extent possible under law, the author(s) have waived all copyright
# and related or neighboring rights to this source code. You can copy, modify,
# distribute and compile the code, even for commercial purposes, all without
# asking permission. 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# 
# For the complete text of the license, please visit
# https://creativecommons.org/publicdomain/zero/1.0/
# ----
#
# REVISIONS and CHANGES 
#    02/10/2023   V1.0   Thomas Forbriger
# 
# ============================================================================
#
#
VERSION=2023-10-02

BEGIN=2023/09/16T12:00:00
END=2023/09/17T12:00:00

dldata.py --network II --location '00' \
  --begin $BEGIN --end $END --outbase KDAK00 \
  --station KDAK R

dldata.py --network II --location '10' \
  --begin $BEGIN --end $END --outbase KDAK10 \
  --station KDAK R

# ----- END OF dldata.sh ----- 
