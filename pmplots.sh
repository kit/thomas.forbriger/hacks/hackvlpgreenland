#!/bin/sh
# this is <pmplots.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2023 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# particle motion plots
# 
# ----
# This program source code is licensed under a CC0 license.
# 
# To the extent possible under law, the author(s) have waived all copyright
# and related or neighboring rights to this source code. You can copy, modify,
# distribute and compile the code, even for commercial purposes, all without
# asking permission. 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# 
# For the complete text of the license, please visit
# https://creativecommons.org/publicdomain/zero/1.0/
# ----
#
# REVISIONS and CHANGES 
#    30/09/2023   V1.0   Thomas Forbriger
# 
# ============================================================================
#
#
VERSION=2023-09-30

BEGIN=2023/09/16T12:30:00
END=2023/09/16T14:00:00
LBHOUR=17T00
LEHOUR=17T01
LBEGIN=2023/09/$LBHOUR:00:00
LEND=2023/09/$LEHOUR:30:00
#END=2023/09/16T20:00:00
SOURCELOC=72.8,-27.0

doplot() {
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $BEGIN \
    --end $END \
    --source=$SOURCELOC \
    --fHP 0.005 \
    --fLP 0.020 --noshow
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $BEGIN \
    --end $END \
    --outbase pmZNE --ZNE \
    --source=$SOURCELOC \
    --fHP 0.005 \
    --fLP 0.020 --noshow
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $BEGIN \
    --end $END \
    --outbase pmnarrow \
    --source=$SOURCELOC \
    --fHP 0.010 \
    --fLP 0.012 --noshow
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $LBEGIN \
    --end $LEND \
    --outbase pmZNEstart$LBHOUR --ZNE \
    --source=$SOURCELOC \
    --fHP 0.005 \
    --fLP 0.020 --noshow
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $LBEGIN \
    --end $LEND \
    --outbase pm_start$LBHOUR \
    --source=$SOURCELOC \
    --fHP 0.005 \
    --fLP 0.020 --noshow
}

doplot II KDAK
doplot IU KEV
doplot IU KONO
doplot IU TIXI
doplot II ALE
doplot DK SCO
doplot GE SUMG
doplot II BFO
doplot II BORG
doplot IU ANMO
doplot IU ULN
doplot IU PAB
doplot IU COLA
doplot IU KBS
doplot IU SFJD
doplot IU SSPA
exit

doplot II BFO
doplot II XBFO
doplot AK H16K
doplot AK U33K
doplot AK S32K
doplot AK BAGL
doplot AK L14K
doplot AK D17K
doplot AK F18K
doplot AK E19K
doplot AK D20K
doplot AK C21K
doplot AK E24K
doplot AK H22K
doplot AK E27K 

exit

# ----- END OF pmplots.sh ----- 
