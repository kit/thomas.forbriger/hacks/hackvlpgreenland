#!/bin/sh
# this is <pmplots1st.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2023 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# particle motion plots for first overtone at 21.6 mHz
# 
# ----
# This program source code is licensed under a CC0 license.
# 
# To the extent possible under law, the author(s) have waived all copyright
# and related or neighboring rights to this source code. You can copy, modify,
# distribute and compile the code, even for commercial purposes, all without
# asking permission. 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# 
# For the complete text of the license, please visit
# https://creativecommons.org/publicdomain/zero/1.0/
# ----
#
# REVISIONS and CHANGES 
#    30/09/2023   V1.0   Thomas Forbriger
# 
# ============================================================================
#
#
VERSION=2023-09-30

BEGIN=2023/10/11T18:00:00
END=2023/10/11T20:00:00
LBEGIN=2023/10/12T00:30:00
LEND=2023/10/12T02:30:00
#END=2023/09/16T20:00:00
SOURCELOC=72.8,-27.0

doplot() {
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $BEGIN \
    --inbase 20231011 \
    --outbase pm20231011_1st \
    --source=$SOURCELOC \
    --end $END \
    --fHP 0.020 \
    --fLP 0.024 --noshow
  ./plotstation.py --verbose --quantity acceleration $1 $2 \
    --begin $LBEGIN \
    --inbase 20231011 \
    --outbase pm20231011later1st \
    --source=$SOURCELOC \
    --end $LEND \
    --fHP 0.020 \
    --fLP 0.024 --noshow
}

doplot IU KEV
doplot II ALE
doplot DK SCO
doplot II XBFO
doplot IU KONO
doplot II KDAK
doplot IU TIXI
doplot II BFO
doplot II BORG
doplot IU ANMO
doplot IU ULN
doplot IU PAB
doplot IU COLA
doplot IU KBS
doplot IU SFJD
doplot IU SSPA


# ----- END OF pmplots1st.sh ----- 
