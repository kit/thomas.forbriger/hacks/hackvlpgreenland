#!/bin/sh
# this is <pmplots.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2023 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# particle motion plots
# 
# ----
# This program source code is licensed under a CC0 license.
# 
# To the extent possible under law, the author(s) have waived all copyright
# and related or neighboring rights to this source code. You can copy, modify,
# distribute and compile the code, even for commercial purposes, all without
# asking permission. 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# 
# For the complete text of the license, please visit
# https://creativecommons.org/publicdomain/zero/1.0/
# ----
#
# REVISIONS and CHANGES 
#    30/09/2023   V1.0   Thomas Forbriger
# 
# ============================================================================
#
#
set -x
VERSION=2023-09-30

SOURCELOC=72.8,-27.0
FHP=0.005
FLP=0.020

doplot() { 
  network=$1
  station=$2
  begin=$3
  end=$4
  quantity=$5
  datebase=$6
  sourceloc=$7
  para="$8"
  quantid=$(echo $quantity | cut -c 1-3)
  ./plotstation.py --verbose \
    $network $station \
    --begin $begin \
    --end $end \
    --quantity $quantity \
    --inbase ${datebase} \
    --outbase pm${datebase}ZNE${quantid} --ZNE \
    --source=$sourceloc \
    --fHP $FHP \
    --fLP $FLP --noshow $para
  }

plotallquant() {
  network=$1
  station=$2
  begin=$3
  end=$4
  datebase=$5
  sourceloc=$6
  para="$7"
  doplot $network $station $begin $end displacement \
    $datebase $sourceloc "$para"
  doplot $network $station $begin $end acceleration \
    $datebase $sourceloc "$para"
  doplot $network $station $begin $end velocity \
    $datebase $sourceloc "$para"
  }

plotalldate() {
  network=$1
  station=$2
  para="$3"

  begin=2005/08/30T03:30:00
  end=2005/08/30T12:00:00
  datebase=20050830

FHP=0.010
FLP=0.012

#  plotallquant $network $station $begin $end $datebase $SOURCELOC "$para"

#----
  
  begin=2006/08/20T17:30:00
  end=2006/08/20T23:30:00
  datebase=20060820

FHP=0.010
FLP=0.012

#  plotallquant $network $station $begin $end $datebase $SOURCELOC "$para"

#----

  begin=2016/02/12T03:30:00
  end=2016/02/12T07:30:00
  datebase=20160212

FHP=0.010
FLP=0.012

  plotallquant $network $station $begin $end $datebase $SOURCELOC "$para"

#----

FHP=0.005
FLP=0.020

  begin=2017/01/20T06:30:00
  end=2017/01/20T11:30:00
  datebase=20170120

#  plotallquant $network $station $begin $end $datebase $SOURCELOC "$para"

#----

  begin=2023/09/16T12:30:00
  end=2023/09/16T14:00:00
  datebase=20230916

#  plotallquant $network $station $begin $end $datebase $SOURCELOC "$para"

#----

  begin=2023/10/11T16:00:00
  end=2023/10/11T20:45:00
  datebase=20231011

#  plotallquant $network $station $begin $end $datebase $SOURCELOC "$para"
}

PARA="--scalepertrace"
plotalldate II ALE "$PARA"
exit
plotalldate DK SCO "$PARA"
plotalldate GR BFO "$PARA"

# ----- END OF pmplots.sh ----- 
